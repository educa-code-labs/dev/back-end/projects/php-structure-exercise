init:
	echo "Start"
up:
	docker compose up
down:
	docker compose down -v
php:
	docker exec -it educacode-php-structure-exercise bash
fix:
	vendor/bin/php-cs-fixer fix
