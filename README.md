![logo](https://i.imgur.com/YUZzmXm.png)

Project Structure Template
=======================================

[![Licence: MIT](https://img.shields.io/badge/Licence-MIT-green)](LICENCE)
[![Team](https://img.shields.io/badge/Team-General-red)](https://gitlab.com/educa-code-labs/general)

* * *

Este projeto é um modelo para criar outros repositórios. Ao usar este modelo, várias configurações 
já estão predefinidas para que você não perca tempo fazendo-as. Além disso, este repositório estará sempre 
atualizado com os novos padrões utilizados pelo EducaCode Labs.

### Requisitos

Atualmente este projeto não tem nenhum requisito.


### Instruções para criar um projeto a partir desse modelo

1. Ao [criar um projeto](https://gitlab.com/projects/new?namespace_id=56564325) no Gitlab você deve selecionar a opção de importar projeto
2. Você deve selecionar a opção importar projeto de **Repository by URL**
3. Você deve preencher o campo **URL do repositório Git** com o seguinte valor:

```text
https://gitlab.com/educa-code-labs/general/templates/project-structure-template.git
```

4. Deixe em branco os campos **nome de usuário** e **senha**
5. Altere o restante das configurações para se adequar ao seu projeto

### Instalação

A maneira recomendada de instalar este projeto é seguindo estas etapas:

1. Realize o clone do projeto para a sua máquina

```shell
git clone git@gitlab.com:educa-code-labs/general/templates/project-structure-template.git
```

Se você estiver usando as configurações de ambiente do [educa-code-labs](https://gitlab.com/educa-code-labs) recomendamos fazer o clone na seguinte pasta `/home/[seu-usuário]/vhost`.

2. Acessar as pastas do projeto

```shell
cd project-structure-template
make init
```

### Validação das informações

Para iniciar a validação de todas as configurações do seu projeto, utilize o comando abaixo. É recomendado executá-lo a partir da raiz do projeto. Confira o exemplo:

```shell
docker run \
--rm \
--network=host \
-v $(pwd):/usr/src/ \
registry.gitlab.com/educa-code-labs/dev/back-end/projects/educacode-cli-php/cli:v0.0.1 \
/bin/sh -c "educacode && educacode validate --dir=/usr/src/"
```

### Enviar as informações para serem processadas

Utilize o comando abaixo para processar e enviar informações para um servidor que fará a análise e o cadastro dos dados. É necessário fazer as alterações adequadas nas variáveis do ambiente de acordo com as configurações específicas do seu sistema. Confira o exemplo abaixo:

```shell
docker run \
--rm \
--network=host \
-e EDUCACODE_REMOTE_URL="http://127.0.0.1:9000/"  \
-e EDUCACODE_REMOTE_TOKEN="c70a28c91663d7c91a863e61586831f5ed08e620" \
-v $(pwd):/usr/src/ \
registry.gitlab.com/educa-code-labs/dev/back-end/projects/educacode-cli-php/cli:v0.0.1 \
/bin/sh -c "educacode && educacode push --dir=/usr/src/"
```

### Software stack

Esse projeto roda nos seguintes softwares:

- Git 2.33+
- Gitlab 15.4.0-pre

### Changelog

Por favor, veja [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

### Seja um dos contribuidores

Quer fazer parte desse projeto? Clique AQUI e leia [como contribuir](CONTRIBUTING.md).

## Segurança

Se você descobrir algum problema relacionado à segurança, envie um e-mail para reinangabriel1520@gmail.com em vez de usar o issue.

### Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE) para mais detalhes.
