FROM phpdockerio/php:8.2-cli

ARG UNAME=educa-code-runner
ARG UID=1000
ARG GID=1000

RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME

WORKDIR /home/educa-code-runner

RUN chown $UNAME /home/educa-code-runner
RUN chmod 700 /home/educa-code-runner

USER $UNAME
RUN composer global require phpunit/phpunit:^10.0

ENV PATH="${PATH}:/home/educa-code-runner/.composer/vendor/bin"
