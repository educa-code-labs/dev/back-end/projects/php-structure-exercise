<?php

declare(strict_types=1);

namespace App\Exercises\HelloWorld;

use PHPUnit\Framework\TestCase;

class HelloWorldTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        require_once __DIR__ . '/HelloWorld.php';
    }

    public function testHelloWorld(): void
    {
        $this->assertEquals('Hello, World!', helloWorld());
    }
}
